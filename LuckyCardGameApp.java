import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){	
		Scanner scanner = new Scanner(System.in);
		GameManager manager = new GameManager();
		int totalpoints = 0;
		System.out.println("Welcome to my game");
		
		while(manager.getNumberOfCards() > 1 && totalpoints < 5){
			System.out.println(manager);
			totalpoints += manager.calculatePoints();
			System.out.println("Total Points :" + totalpoints);
			manager.dealCards();
		}
		if(manager.getNumberOfCards() < 1){
			System.out.println("You lost, Sad");
		}
		else{
			System.out.println("You won, Congragulations");
		}
	}
}